#!/usr/bin/env python
# -*- coding: utf-8 -*-

import socket
import logging
import pymongo
import time
import signal
import sys
import threading






def help():
    print("Help to this simple server. 3 args are needed: ")
    print("1 - address of MongoDB server.")
    print("2 - port of listener socket of this server.")
    print("3 - name of MongoDB")
    print("Example: SERVER.py 127.0.0.1 9000 PYSERVER_DB")







def working_thread():

    if(len(sys.argv) != 4 ):
        message = '[' + str(time.asctime()) + ']' + " wrong number of args."
        logging.error(message)
        help()
        sys.exit(1)

    host = str(sys.argv[1])
    port = int(sys.argv[2])
    dbname = str(sys.argv[3])

    logging.basicConfig( format = u'%(message)s', level = logging.DEBUG)
    try:
        mongoconn = pymongo.Connection( host=host, network_timeout=3 )
        db = mongoconn[dbname]
    except:
        message = '[' + str(time.asctime()) + ']' + " connection to MongoDB Failed."
        logging.error(message)
        help()
    #db.drop_collection('messages')
    # Получить все документы
    #try:
    #    for user in db.messages.find():
    #        print user
    #except:
    #    print("oops!")
    try:
        sock = socket.socket()
        sock.bind(('', port))
        sock.listen(1)
    except:
        message = '[' + str(time.asctime()) + ']' + " creating listen socket failed."
        logging.error(message)
        help()
    while True:
        conn, addr = sock.accept()
        data = conn.recv(20)
        currenttime = '[' + str(time.asctime()) + ']'
        message = currenttime + " ip = '" + addr[0] + "' port = '" + str(addr[1]) + "' message = '" + data
        logging.info(message)
        try:
            db.messages.save( { 'timestamp':currenttime, 'ip':addr[0], 'port':str(addr[1]), 'message':data } )
            conn.send("OK")
        except:
            conn.send("FAIL")
        conn.close()









def run_program():
    t = threading.Thread(target=working_thread, args=())
    t.daemon = True
    t.start()
    while True:
        time.sleep(1)








def exit_handler(signum, frame):
    # restore the original signal handler as otherwise evil things will happen
    # in raw_input when CTRL+C is pressed, and our signal handler is not re-entrant
    signal.signal(signal.SIGINT, original_sigint)
    signal.signal(signal.SIGTERM, original_sigterm)
    sys.exit(1)







if __name__ == '__main__':
    # store the original SIGINT handler
    original_sigint = signal.getsignal(signal.SIGINT)
    original_sigterm = signal.getsignal(signal.SIGTERM)
    signal.signal(signal.SIGINT, exit_handler)
    signal.signal(signal.SIGTERM, exit_handler)
    run_program()