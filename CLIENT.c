#include <stdio.h>
#include <winsock2.h>  // Wincosk2.h ������ ����
      // ��������� ������ windows.h!
#include <windows.h>
#include <time.h>

unsigned help()
{
    printf("Simple client's help.\nTo run client, enter 3 arguments.\n");
    printf("1 - number of messages to send to server\n");
    printf("2 - ip address of server\n");
    printf("3 - port of server\n");
    printf("Example : CLIENT.exe 43 127.0.0.1 9000\n\n");
    return 1;
}

unsigned isnumber( char*arg )
{
    if(*arg=='0')
        return 0;

    for(;(*arg)!='\0';arg++)
        if(!isdigit(*arg))
            return 0;

    return 1;
}

int main(int argc, char**argv)
{
    srand(time(NULL));
    unsigned i, j = 0;
    int traffic = 0;
    int err = 0;

    if(argc!=4)
        return help();
    if( !(isnumber(argv[1])==1 && isnumber(argv[3])==1) )
        return help();

    unsigned serverport = atoi(argv[3]);
    unsigned numberofmessages = atoi(argv[1]);

    WSADATA WsaData; //������ ������������� ��� ����������
    SOCKET client; //����������, ������
    struct sockaddr_in localaddr; //���������-����� ��� �������
    WSAStartup(MAKEWORD(2,2), &WsaData);

    localaddr.sin_family = AF_INET;
    localaddr.sin_port = htons( serverport );
    localaddr.sin_addr.S_un.S_addr = inet_addr( argv[2] );
    unsigned localaddr_size = sizeof(localaddr);

    char buffer[21];

    for (j=0;j<numberofmessages;j++)
    {
        system("timeout 1");
        client = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, 0);
        if(SOCKET_ERROR == connect(client, (struct sockaddr*)&localaddr, localaddr_size))
        {
            printf("Socket connection failed.\n");
            closesocket(client);
            err = 1;
            continue;
        }

        for (i=0;i<20;i++)
            buffer[i] = 65 + rand()%26;
        buffer[20] = '\0';

        traffic = send(client, buffer, 20, 0); //���������� 20 ������-��������� ��������
        if(traffic == SOCKET_ERROR)
        {
            printf("ERROR. Send function failed.\nMessage #%d was not saved in DB.\n", j+1);
            closesocket(client);
            err = 1;
            continue;
        }
        printf("%d bytes was sent. Message :\n%s\nwas sent.\n", traffic, buffer);
        memset(buffer,'\0',21);
        traffic = recv(client, buffer, 4, 0); //�������� OK ��� FAIL
        if(traffic == SOCKET_ERROR)
        {
            printf("ERROR. Receive function failed.\nMessage #%d was not saved in DB.\n", j+1);
            closesocket(client);
            err = 1;
            continue;
        }
        printf("Received %d bytes.\n", traffic);
        printf("Message :\n%s\nwas received.\n", buffer);
        closesocket(client);

        if( memcmp(buffer, "FAIL", 4) == 0 )
        {
            printf("ERROR. Message #%d was not saved in DB.\n", j+1);
            err = 1;
        }
        if( memcmp(buffer, "OK", 2) == 0 )
            printf("SUCCESS. Message #%d was saved in DB.\n", j+1);

    }

    WSACleanup();

    return err;
}
